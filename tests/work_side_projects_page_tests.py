from lib.base_test_class import BaseTestClass
from lib.pages.work_side_projects_page import WorkSideProjectsPage
import time

class WorkSideProjectsPageTests(BaseTestClass):

	def test_work_side_projects_page_is_on_current_page(self):
		side_projects = WorkSideProjectsPage();
		side_projects.go_to_page(self.driver);
		self.assertTrue(side_projects.is_on_current_page(self.driver));

	def test_work_side_project_page_menu_links_exist(self):
		side_projects = WorkSideProjectsPage();
		side_projects.go_to_page(self.driver);
		self.assertTrue(side_projects.is_element_exist(self.driver, 'about_me_menu_link'));
		self.assertTrue(side_projects.is_element_exist(self.driver, 'about_me_menu_link'));
		self.assertTrue(side_projects.is_element_exist(self.driver, 'work_menu_link'));
		self.assertTrue(side_projects.is_element_exist(self.driver, 'work_experience_submenu_link'));
		self.assertTrue(side_projects.is_element_exist(self.driver, 'work_side_projects_submenu_link'));
		self.assertTrue(side_projects.is_element_exist(self.driver, 'blog_menu_link'));
		self.assertTrue(side_projects.is_element_exist(self.driver, 'contact_me_menu_link'));
