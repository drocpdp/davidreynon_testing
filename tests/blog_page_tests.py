from lib.base_test_class import BaseTestClass
from lib.pages.blog_page import BlogPage
import time

class BlogPageTests(BaseTestClass):

	def test_blog_page_is_on_current_page(self):
		blog = BlogPage();
		blog.go_to_page(self.driver);
		self.assertTrue(blog.is_on_current_page(self.driver));

	def test_blog_page_menu_links_exist(self):
		blog = BlogPage();
		blog.go_to_page(self.driver);
		self.assertTrue(blog.is_element_exist(self.driver, 'about_me_menu_link'));
		self.assertTrue(blog.is_element_exist(self.driver, 'about_me_menu_link'));
		self.assertTrue(blog.is_element_exist(self.driver, 'work_menu_link'));
		self.assertTrue(blog.is_element_exist(self.driver, 'work_experience_submenu_link'));
		self.assertTrue(blog.is_element_exist(self.driver, 'work_side_projects_submenu_link'));
		self.assertTrue(blog.is_element_exist(self.driver, 'blog_menu_link'));
		self.assertTrue(blog.is_element_exist(self.driver, 'contact_me_menu_link'));

	def test_blog_page_recent_posts_validate_link_0(self):
		link_index = 0;
		blog = BlogPage();
		blog.go_to_page(self.driver);
		post_text = blog.get_nth_post_text_from_recent_posts_list(self.driver, link_index);
		blog.click_nth_post_from_recent_posts_list(self.driver, link_index);
		blog_entry_title = blog.get_blog_entry_entry_title_text(self.driver);
		self.assertEqual(post_text, blog_entry_title);

	def test_blog_page_recent_posts_validate_link_1(self):
		link_index = 1;
		blog = BlogPage();
		blog.go_to_page(self.driver);
		post_text = blog.get_nth_post_text_from_recent_posts_list(self.driver, link_index);
		blog.click_nth_post_from_recent_posts_list(self.driver, link_index);
		blog_entry_title = blog.get_blog_entry_entry_title_text(self.driver);
		self.assertEqual(post_text, blog_entry_title);		

	def test_blog_page_recent_posts_validate_link_2(self):
		link_index = 2;
		blog = BlogPage();
		blog.go_to_page(self.driver);
		post_text = blog.get_nth_post_text_from_recent_posts_list(self.driver, link_index);
		blog.click_nth_post_from_recent_posts_list(self.driver, link_index);
		blog_entry_title = blog.get_blog_entry_entry_title_text(self.driver);
		self.assertEqual(post_text, blog_entry_title);		

	def test_blog_page_recent_posts_validate_link_3(self):
		link_index = 3;
		blog = BlogPage();
		blog.go_to_page(self.driver);
		post_text = blog.get_nth_post_text_from_recent_posts_list(self.driver, link_index);
		blog.click_nth_post_from_recent_posts_list(self.driver, link_index);
		blog_entry_title = blog.get_blog_entry_entry_title_text(self.driver);
		self.assertEqual(post_text, blog_entry_title);		

	def test_blog_page_recent_posts_validate_link_4(self):
		link_index = 4;
		blog = BlogPage();
		blog.go_to_page(self.driver);
		post_text = blog.get_nth_post_text_from_recent_posts_list(self.driver, link_index);
		blog.click_nth_post_from_recent_posts_list(self.driver, link_index);
		blog_entry_title = blog.get_blog_entry_entry_title_text(self.driver);
		self.assertEqual(post_text, blog_entry_title);

	def test_blog_page_archives_links_contain_articles_within_selected_month_year(self):
		''' Used for section block Archives, which contains a list of links that
		contain posts sorted by its particular month/date. From blog page, format of 
		list is Month YYYY example: January 2018
		Upon clicking of each month/date, text on footer of each article of the feed is
		"This entry was posted.... on January 2, 2018"
		We loop through each archive link, retain Month and Year, click archive link,
		then loop through each article footer in feed, obtaining and comparing Month and Year
		'''
		blog = BlogPage();
		blog.go_to_page(self.driver);
		number_archives = len(blog.get_archives_list(self.driver));
		# loop through each archive link
		for index in range(0,number_archives):
			# get month and year value
			expected_month = blog.get_nth_archive_text_from_archives_list(self.driver, index).split(' ')[0];
			expected_year = blog.get_nth_archive_text_from_archives_list(self.driver, index).split(' ')[1];
			# click the archive link
			blog.click_nth_archive_from_archives_list(self.driver,index);
			# land on feed, loop through each article (the footer entry date) in feed
			for entry_date in blog.get_feed_article_footer_entry_dates(self.driver):
				split_date = entry_date.text.split(' ');
				# compare
				self.assertEqual(split_date[0], expected_month);
				self.assertEqual(split_date[2], expected_year);
			# click menu to re-land at blog page home				
			blog.click_blog_menu_link(self.driver);
	
	def test_blog_page_archives_links_contain_feed_title_displaying_selected_month_year(self):
		''' Used for section block Archives, which contains a list of links that
		contain posts sorted by its particular month/date. From blog page, format of 
		list is Month YYYY example: January 2018
		Upon clicking of each month/date, title of feed list should be: "MONTHLY ARCHIVES: APRIL 2018"
		Character case of list and title differs, so switch all to lowercase prior to compare
		'''
		blog = BlogPage();
		blog.go_to_page(self.driver);
		number_archives = len(blog.get_archives_list(self.driver));
		# loop through each archive link
		for index in range(0,number_archives):
			# get month and year value (switch to lowercase)
			expected_month = blog.get_nth_archive_text_from_archives_list(self.driver, index).split(' ')[0].lower();
			expected_year = blog.get_nth_archive_text_from_archives_list(self.driver, index).split(' ')[1].lower();
			# click the archive link
			blog.click_nth_archive_from_archives_list(self.driver,index);
			actual_month = blog.get_feed_page_archive_title_text(self.driver).split(' ')[2].lower();
			actual_year = blog.get_feed_page_archive_title_text(self.driver).split(' ')[3].lower();
			self.assertEqual(expected_month, actual_month);
			self.assertEqual(expected_year, actual_year);
			# click menu to re-land at blog page home				
			blog.click_blog_menu_link(self.driver);	

	def test_blog_page_does_categories_dropdown_exist(self):
		''' Simple test to see if dropdown exists. (It is possible that user could inadvertantly
		remove this while editing page)
		'''
		blog = BlogPage();
		blog.go_to_page(self.driver);
		self.assertTrue(blog.is_categories_dropdown_exist(self.driver));

	def test_blog_page_select_categories_dropdown_validate_article_contains_hyperlinked_selected_category_select_by_index(self):
		''' Select category from dropdown. Scan all shown articles for selected category
		'''
		blog = BlogPage();
		blog.go_to_page(self.driver);
		number_of_options = blog.categories_dropdown_get_number_options(self.driver);
		# start at index 1 to skip on-landing instruction text located in index 0
		for index in range(1, number_of_options):
			expected_category = blog.categories_dropdown_get_all_selectable_options(self.driver)[index].text;
			blog.categories_dropdown_select_by_index(self.driver, index);
			footers = blog.get_feed_article_footers(self.driver);
			for footer in footers:
				self.assertIn(expected_category, blog.categories_extract_categories_from_footer_text(footer.text));
			# click blog page to re-land at blog home
			blog.click_blog_menu_link(self.driver);

	def test_blog_page_select_categories_dropdown_validate_article_contains_hyperlinked_selected_category_select_by_name(self):
		''' Select category from dropdown. Loop through all resultant shown articles for selected category
		'''
		blog = BlogPage();
		blog.go_to_page(self.driver);
		number_of_options = blog.categories_dropdown_get_number_options(self.driver);
		# start at index 1 to skip on-landing instruction text located in index 0
		for index in range(1, number_of_options):
			expected_category = blog.categories_dropdown_get_all_selectable_options(self.driver)[index].text;
			blog.categories_dropdown_select_by_value(self.driver, expected_category);
			footers = blog.get_feed_article_footers(self.driver);
			for footer in footers:
				self.assertIn(expected_category, blog.categories_extract_categories_from_footer_text(footer.text));
			# click blog page to re-land at blog home
			blog.click_blog_menu_link(self.driver);

	def test_blog_page_search_is_search_form_field_exist(self):
		''' Checks that search entry form and submit button exist
		'''
		blog = BlogPage();
		blog.go_to_page(self.driver);
		self.assertTrue(blog.is_search_entry_field_exists(self.driver));
		self.assertTrue(blog.is_search_submit_button_exists(self.driver));

	def test_blog_page_search_non_matching_entry_returns_no_results(self):
		blog = BlogPage();
		blog.go_to_page(self.driver);
		blog.search_set_search_entry(self.driver, 'asdfasdfzzzzzz');
		blog.search_submit_search(self.driver);
		self.assertEqual(blog.get_blog_entry_entry_title_text(self.driver), 'Nothing Found');

	def test_blog_page_article_and_title_contains_search_term_entered_title_only(self):
		''' Checks for a search term that is only contained in the title, not the article
		'''
		search_entry = 'utilizing';
		blog = BlogPage();
		blog.go_to_page(self.driver);
		blog.search_set_search_entry(self.driver, search_entry);
		blog.search_submit_search(self.driver);
		search_results = blog.get_search_search_results_article_hyperlinks(self.driver);
		for article in search_results:
			self.driver.get(article);
			title_text = blog.get_blog_entry_entry_title_text(self.driver).lower();
			article_text = blog.get_content_text(self.driver).lower();
			search_text = title_text + article_text;
			self.assertIn(search_entry.lower(), search_text);

	def test_blog_page_article_and_title_contains_search_term_entered_article_only(self):
		''' Checks for a search term that is only contained in the article, not the title
		'''
		search_entry = 'ActionChain';
		blog = BlogPage();
		blog.go_to_page(self.driver);
		blog.search_set_search_entry(self.driver, search_entry);
		blog.search_submit_search(self.driver);
		search_results = blog.get_search_search_results_article_hyperlinks(self.driver);
		for article in search_results:
			self.driver.get(article);
			title_text = blog.get_blog_entry_entry_title_text(self.driver).lower();
			article_text = blog.get_content_text(self.driver).lower();
			search_text = title_text + article_text;
			self.assertIn(search_entry.lower(), search_text);

	def test_blog_page_article_and_title_contains_search_term_mixed_case(self):
		''' Checks for a search term that is contained in both the title and the article
		entry is mixed case
		'''
		search_entry = 'uTiLiZiNg';
		blog = BlogPage();
		blog.go_to_page(self.driver);
		blog.search_set_search_entry(self.driver, search_entry);
		blog.search_submit_search(self.driver);
		search_results = blog.get_search_search_results_article_hyperlinks(self.driver);
		for article in search_results:
			self.driver.get(article);
			title_text = blog.get_blog_entry_entry_title_text(self.driver).lower();
			article_text = blog.get_content_text(self.driver).lower();
			search_text = title_text + article_text;
			self.assertIn(search_entry.lower(), search_text);

	def test_blog_page_article_and_title_contains_search_term_special_characters(self):
		''' Checks for a search term that is contained in both the title and the article
		entry is special characters
		'''
		search_entry = '*.pyc';
		blog = BlogPage();
		blog.go_to_page(self.driver);
		blog.search_set_search_entry(self.driver, search_entry);
		blog.search_submit_search(self.driver);
		search_results = blog.get_search_search_results_article_hyperlinks(self.driver);
		for article in search_results:
			self.driver.get(article);
			title_text = blog.get_blog_entry_entry_title_text(self.driver).lower();
			article_text = blog.get_content_text(self.driver).lower();
			search_text = title_text + article_text;
			self.assertIn(search_entry.lower(), search_text);	