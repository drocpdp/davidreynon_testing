from lib.base_test_class import BaseTestClass
from lib.pages.work_experience_page import WorkExperiencePage
import time

class WorkExperiencePageTests(BaseTestClass):

	def test_work_experience_page_is_on_current_page(self):
		work_experience = WorkExperiencePage();
		work_experience.go_to_page(self.driver);
		self.assertTrue(work_experience.is_on_current_page(self.driver));

	def test_linked_in_hard_link_on_work_experience_page_no_navigation(self):
		work_experience_page = WorkExperiencePage()
		work_experience_page.go_to_page(self.driver);
		full_content_text = work_experience_page.get_content_text(self.driver)
		word_array = full_content_text.split()
		url = ''
		for word in word_array:
			if 'http' in word:
				url = word
		print (url)
		self.assertEqual(url, work_experience_page.get_property('linkedin_url'))

	def test_work_experience_page_menu_links_exist(self):
		work_experience = WorkExperiencePage();
		work_experience.go_to_page(self.driver);
		self.assertTrue(work_experience.is_element_exist(self.driver, 'about_me_menu_link'));
		self.assertTrue(work_experience.is_element_exist(self.driver, 'about_me_menu_link'));
		self.assertTrue(work_experience.is_element_exist(self.driver, 'work_menu_link'));
		self.assertTrue(work_experience.is_element_exist(self.driver, 'work_experience_submenu_link'));
		self.assertTrue(work_experience.is_element_exist(self.driver, 'work_side_projects_submenu_link'));
		self.assertTrue(work_experience.is_element_exist(self.driver, 'blog_menu_link'));
		self.assertTrue(work_experience.is_element_exist(self.driver, 'contact_me_menu_link'));		
