from lib.base_test_class import BaseTestClass
from lib.pages.contact_me_page import ContactMePage
from selenium.webdriver.support.wait import WebDriverWait
import time

class ContactMePageTests(BaseTestClass):

	def test_contact_me_page_is_on_current_page(self):
		contact_me = ContactMePage();
		contact_me.go_to_page(self.driver);
		self.assertTrue(contact_me.is_on_current_page(self.driver));

	def test_contact_me_page_menu_links_exist(self):
		contact = ContactMePage();
		contact.go_to_page(self.driver);
		self.assertTrue(contact.is_element_exist(self.driver, 'about_me_menu_link'));
		self.assertTrue(contact.is_element_exist(self.driver, 'about_me_menu_link'));
		self.assertTrue(contact.is_element_exist(self.driver, 'work_menu_link'));
		self.assertTrue(contact.is_element_exist(self.driver, 'work_experience_submenu_link'));
		self.assertTrue(contact.is_element_exist(self.driver, 'work_side_projects_submenu_link'));
		self.assertTrue(contact.is_element_exist(self.driver, 'blog_menu_link'));
		self.assertTrue(contact.is_element_exist(self.driver, 'contact_me_menu_link'));

	def test_contact_me_validate_blank_required_your_name_entry(self):
		contact = ContactMePage();
		contact.go_to_page(self.driver);
		contact.click_form_field_submit_button(self.driver);
		contact.wait_for_exists(self.driver, 'summary_validation_errors_occurred');
		self.assertTrue(contact.is_your_name_form_field_validation_text_exists(self.driver));
		self.assertEqual(contact.get_your_name_form_field_validation_text(self.driver), contact.get_full_property_value('validation_message_required'));

	def test_contact_me_validate_blank_required_your_email_entry(self):
		contact = ContactMePage();
		contact.go_to_page(self.driver);
		contact.click_form_field_submit_button(self.driver);
		contact.wait_for_exists(self.driver, 'summary_validation_errors_occurred');
		self.assertTrue(contact.is_your_email_form_field_validation_text_exists(self.driver));
		self.assertEqual(contact.get_your_email_form_field_validation_text(self.driver), contact.get_full_property_value('validation_message_required'));

	def test_contact_me_validate_blank_required_your_message_entry(self):
		contact = ContactMePage();
		contact.go_to_page(self.driver);
		contact.click_form_field_submit_button(self.driver);
		contact.wait_for_exists(self.driver, 'summary_validation_errors_occurred');
		self.assertTrue(contact.is_your_message_form_field_validation_text_exists(self.driver));
		self.assertEqual(contact.get_your_message_form_field_validation_text(self.driver), contact.get_full_property_value('validation_message_required'));

	def test_contact_me_validate_blank_required_captcha_entry(self):
		contact = ContactMePage();
		contact.go_to_page(self.driver);
		contact.click_form_field_submit_button(self.driver);
		contact.wait_for_exists(self.driver, 'summary_validation_errors_occurred');
		self.assertTrue(contact.is_captcha_form_field_validation_text_exists(self.driver));
		self.assertEqual(contact.get_captcha_form_field_validation_text(self.driver), contact.get_full_property_value('validation_message_captcha_incorrect'));

	def test_contact_me_validate_entered_required_your_name_entry(self):
		contact = ContactMePage();
		contact.go_to_page(self.driver);
		contact.set_your_name_form_field(self.driver, 'Test Name');
		contact.click_form_field_submit_button(self.driver);
		contact.wait_for_exists(self.driver, 'summary_validation_errors_occurred');
		self.assertFalse(contact.is_your_name_form_field_validation_text_exists(self.driver));

	def test_contact_me_validate_entered_required_your_email_entry(self):
		contact = ContactMePage();
		contact.go_to_page(self.driver);
		contact.set_your_email_form_field(self.driver, 'testemail@email.com');
		contact.click_form_field_submit_button(self.driver);
		contact.wait_for_exists(self.driver, 'summary_validation_errors_occurred');
		self.assertFalse(contact.is_your_email_form_field_validation_text_exists(self.driver));

	def test_contact_me_validate_entered_required_your_message_entry(self):
		contact = ContactMePage();
		contact.go_to_page(self.driver);
		contact.set_your_message_form_field(self.driver, 'Test Your Message 1');
		contact.click_form_field_submit_button(self.driver); 
		contact.wait_for_exists(self.driver, 'summary_validation_errors_occurred'); 
		self.assertFalse(contact.is_your_message_form_field_validation_text_exists(self.driver)); 

	def test_contact_me_validate_entered_invalid_format_leading_ampersand_your_email_entry(self):
		contact = ContactMePage();
		contact.go_to_page(self.driver);
		contact.set_your_email_form_field(self.driver, '@email.com');
		contact.click_form_field_submit_button(self.driver);
		contact.wait_for_exists(self.driver, 'summary_validation_errors_occurred');
		self.assertTrue(contact.is_your_email_form_field_validation_text_exists(self.driver));
		self.assertEqual(contact.get_your_email_form_field_validation_text(self.driver), contact.get_full_property_value('validation_message_email_invalid'));

	def test_contact_me_validate_entered_invalid_format_no_ampersand_your_email_entry(self):
		contact = ContactMePage();
		contact.go_to_page(self.driver);
		contact.set_your_email_form_field(self.driver, 'testemail.com');
		contact.click_form_field_submit_button(self.driver);
		contact.wait_for_exists(self.driver, 'summary_validation_errors_occurred');
		self.assertTrue(contact.is_your_email_form_field_validation_text_exists(self.driver));
		self.assertEqual(contact.get_your_email_form_field_validation_text(self.driver), contact.get_full_property_value('validation_message_email_invalid'));

	def test_contact_me_validate_entered_invalid_format_no_dot_your_email_entry(self):
		contact = ContactMePage();
		contact.go_to_page(self.driver);
		contact.set_your_email_form_field(self.driver, 'test@emailcom');
		contact.click_form_field_submit_button(self.driver);
		contact.wait_for_exists(self.driver, 'summary_validation_errors_occurred');
		self.assertTrue(contact.is_your_email_form_field_validation_text_exists(self.driver));
		self.assertEqual(contact.get_your_email_form_field_validation_text(self.driver), contact.get_full_property_value('validation_message_email_invalid'));

	def test_contact_me_validate_entered_invalid_format_no_ampersand_no_dot_your_email_entry(self):
		contact = ContactMePage();
		contact.go_to_page(self.driver);
		contact.set_your_email_form_field(self.driver, 'testemailcom');
		contact.click_form_field_submit_button(self.driver);
		contact.wait_for_exists(self.driver, 'summary_validation_errors_occurred');
		self.assertTrue(contact.is_your_email_form_field_validation_text_exists(self.driver));
		self.assertEqual(contact.get_your_email_form_field_validation_text(self.driver), contact.get_full_property_value('validation_message_email_invalid'));

	def test_contact_me_validate_entered_invalid_format_reverse_order_ampersand_and_dot_your_email_entry(self):
		contact = ContactMePage();
		contact.go_to_page(self.driver);
		contact.set_your_email_form_field(self.driver, 'test.email@com');
		contact.click_form_field_submit_button(self.driver);
		contact.wait_for_exists(self.driver, 'summary_validation_errors_occurred');
		self.assertTrue(contact.is_your_email_form_field_validation_text_exists(self.driver));
		self.assertEqual(contact.get_your_email_form_field_validation_text(self.driver), contact.get_full_property_value('validation_message_email_invalid'));
