from lib.base_test_class import BaseTestClass
from lib.pages.about_me_page import AboutMePage
from lib.pages.work_page import WorkPage
from lib.pages.blog_page import BlogPage
from lib.pages.contact_me_page import ContactMePage
from lib.pages.work_experience_page import WorkExperiencePage
from lib.pages.work_side_projects_page import WorkSideProjectsPage
from lib.pages.menu_page import MenuPage
import time

class WorkPageMenuSelectionTests(BaseTestClass):

	def test_menu_selection_select_from_contact_me(self):
		work = WorkPage();
		about_me = AboutMePage();
		work.go_to_page(self.driver);
		MenuPage().click_about_me_menu_link(self.driver);
		self.assertTrue(about_me.is_on_current_page(self.driver));

	def test_menu_selection_select_work_from_contact_me(self):
		work = WorkPage();
		work.go_to_page(self.driver);
		MenuPage().click_work_menu_link(self.driver);
		self.assertTrue(work.is_on_current_page(self.driver));

	def test_menu_selection_select_blog_from_contact_me(self):
		work = WorkPage();
		blog = BlogPage();
		work.go_to_page(self.driver);
		MenuPage().click_blog_menu_link(self.driver);
		self.assertTrue(blog.is_on_current_page(self.driver));

	def test_menu_selection_select_contact_me_from_contact_me(self):
		work = WorkPage();
		contact_me = ContactMePage();
		work.go_to_page(self.driver);
		MenuPage().click_contact_me_menu_link(self.driver);
		self.assertTrue(contact_me.is_on_current_page(self.driver));

	def test_menu_selection_select_work_experience_submenu_from_contact_me(self):
		work = WorkPage();
		work_experience = WorkExperiencePage();
		work.go_to_page(self.driver);
		MenuPage().click_work_experience_menu_link(self.driver);
		self.assertTrue(work_experience.is_on_current_page(self.driver));

	def test_menu_selection_select_work_side_projects_submenu_from_contact_me(self):
		work = WorkPage();
		side_projects = WorkSideProjectsPage();
		work.go_to_page(self.driver);
		MenuPage().click_side_projects_menu_link(self.driver);
		self.assertTrue(side_projects.is_on_current_page(self.driver));		
