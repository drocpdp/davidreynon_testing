from lib.base_test_class import BaseTestClass
from lib.pages.work_page import WorkPage
import time

class WorkPageTests(BaseTestClass):

	def test_work_page_is_on_current_page(self):
		work = WorkPage();
		work.go_to_page(self.driver);
		self.assertTrue(work.is_on_current_page(self.driver));

	def test_work_page_menu_links_exist(self):
		work = WorkPage();
		work.go_to_page(self.driver);
		self.assertTrue(work.is_element_exist(self.driver, 'about_me_menu_link'));
		self.assertTrue(work.is_element_exist(self.driver, 'about_me_menu_link'));
		self.assertTrue(work.is_element_exist(self.driver, 'work_menu_link'));
		self.assertTrue(work.is_element_exist(self.driver, 'work_experience_submenu_link'));
		self.assertTrue(work.is_element_exist(self.driver, 'work_side_projects_submenu_link'));
		self.assertTrue(work.is_element_exist(self.driver, 'blog_menu_link'));
		self.assertTrue(work.is_element_exist(self.driver, 'contact_me_menu_link'));