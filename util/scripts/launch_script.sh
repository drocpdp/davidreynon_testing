#!/bin/bash
export BASE_DIRECTORY=/opt/testing
export PROJECT_NAME=test_framework_template
export PROJECT_DIRECTORY=$BASE_DIRECTORY/$PROJECT_NAME
export CURRENT_REPORT_OUTPUT=$BASE_DIRECTORY/reports/$PROJECT_NAME.html
export ARCHIVE_REPORT_OUTPUT_DIRECTORY=$BASE_DIRECTORY/reports/old_reports/
source $BASE_DIRECTORY/virtualenvs/test_automation_frameworks/bin/activate
export PATH=/Library/Frameworks/Python.framework/Versions/3.6/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin
export PYTHONPATH=$PROJECT_DIRECTORY
export EMAILER_PROPERTIES_FILE=$BASE_DIRECTORY/configs/$PROJECT_NAME.properties
export EMAILER_REPORT_LOCATION=$BASE_DIRECTORY/reports/$PROJECT_NAME.html

#$PROJECT_DIRECTORY/util/sauce_connect/bin/sc -u $SAUCE_USERNAME -k $SAUCE_ACCESS_KEY -i sc-proxy-tunnel &
/opt/testing/virtualenvs/test_automation_frameworks/bin/nosetests --with-html --html-file=$CURRENT_REPORT_OUTPUT $PROJECT_DIRECTORY
/usr/bin/python3 $PROJECT_DIRECTORY/util/emailer.py

file_name=$PROJECT_NAME.html
current_time=$(date "+%Y.%m.%d-%H.%M.%S")
new_fileName=$file_name.$current_time
mv $CURRENT_REPORT_OUTPUT $ARCHIVE_REPORT_OUTPUT_DIRECTORY/$new_fileName
