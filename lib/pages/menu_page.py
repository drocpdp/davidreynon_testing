import selenium
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from lib.base_page import BasePage
import time

class MenuPage(BasePage):

	''' Global Page designed for top global menu.
	Any test class can call these methods statically.
	'''

	PROPERTIES_FILE = "menu_page.properties"

	def get_about_me_menu_link(self, driver):
		return self.get_element(driver, "about_me_menu_link")

	def get_work_menu_link(self, driver):
		return self.get_element(driver, "work_menu_link")

	def get_work_experience_submenu_link(self,driver):
		return self.get_element(driver, "work_experience_submenu_link")

	def get_work_side_projects_submenu_link(self, driver):
		return self.get_element(driver, "work_side_projects_submenu_link");

	def get_blog_menu_link(self, driver):
		return self.get_element(driver, "blog_menu_link");

	def get_contact_me_menu_link(self, driver):
		return self.get_element(driver, "contact_me_menu_link");

	def click_about_me_menu_link(self, driver):
		self.get_about_me_menu_link(driver).click()		

	def click_work_menu_link(self, driver):
		self.get_work_menu_link(driver).click()

	def click_blog_menu_link(self, driver):
		self.get_blog_menu_link(driver).click()

	def click_contact_me_menu_link(self, driver):
		self.get_contact_me_menu_link(driver).click();

	def click_work_experience_menu_link(self, driver):
		action = ActionChains(driver)
		action.move_to_element(self.get_work_menu_link(driver))
		action.perform();
		self.get_work_experience_submenu_link(driver).click();

	def click_side_projects_menu_link(self, driver):
		action = ActionChains(driver)
		action.move_to_element(self.get_work_menu_link(driver))
		action.perform();
		self.get_work_side_projects_submenu_link(driver).click();

