import selenium
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.support.select import Select
from lib.base_page import BasePage
import time

class BlogPage(BasePage):

	PROPERTIES_FILE = "blog_page.properties"

	def get_content(self, driver):
		return self.get_element(driver, "entry_content")

	def get_content_text(self, driver):
		return self.get_content(driver).text;

	def is_on_current_page(self, driver):
		"""overrides method in /pages/base_page.py - BasePage()"""
		if self.is_element_exist(driver, 'is_on_current_page_indicator_element'):
			return True
		return False;		

	def get_recent_posts_list(self, driver):
		return self.get_elements(driver, 'recent_posts_list');

	def get_nth_post_from_recent_posts_list(self, driver, index):
		posts_list = self.get_recent_posts_list(driver);
		return posts_list[index];

	def click_nth_post_from_recent_posts_list(self, driver, index):
		self.get_nth_post_from_recent_posts_list(driver, index).click();

	def get_nth_post_text_from_recent_posts_list(self, driver, index):
		post_obj = self.get_nth_post_from_recent_posts_list(driver, index);
		return post_obj.text;

	def get_blog_entry_entry_title_element(self, driver):
		return self.get_element(driver, 'entry_entry_title_element');

	def get_blog_entry_entry_title_text(self, driver):
		return self.get_blog_entry_entry_title_element(driver).text;

	def get_archives_list(self, driver):
		return self.get_elements(driver, 'archives_list');

	def get_nth_archive_from_archives_list(self, driver, index):
		archives_list = self.get_archives_list(driver);
		return archives_list[index];

	def click_nth_archive_from_archives_list(self, driver, index):
		self.get_nth_archive_from_archives_list(driver, index).click();

	def get_nth_archive_text_from_archives_list(self, driver, index):
		archive_obj = self.get_nth_archive_from_archives_list(driver, index);
		return archive_obj.text;

	def get_feed_article_footers(self, driver):
		return self.get_elements(driver, 'entry_footer');

	def get_feed_article_footer_entry_dates(self, driver):
		return self.get_elements(driver, 'feed_article_footer_entry_dates');

	def get_feed_page_archive_title_element(self, driver):
		return self.get_element(driver, 'feed_page_archive_title');

	def get_feed_page_archive_title_text(self, driver):
		return self.get_feed_page_archive_title_element(driver).text;

	def get_categories_dropdown(self, driver):
		return self.get_element(driver, 'categories_dropdown');

	def is_categories_dropdown_exist(self, driver):
		if self.get_elements(driver, 'categories_dropdown'):
			return True;
		return False;

	def categories_dropdown_get_all_selectable_options(self, driver):
		return Select(self.get_element(driver, 'categories_dropdown')).options;

	def categories_dropdown_get_number_options(self, driver):
		return len(self.categories_dropdown_get_all_selectable_options(driver));	
		
	def categories_dropdown_select_by_value(self, driver, value):
		return Select(self.get_element(driver, 'categories_dropdown')).select_by_visible_text(value);

	def categories_dropdown_select_by_index(self, driver, index_value):
		return Select(self.get_element(driver, 'categories_dropdown')).select_by_index(index_value);

	def categories_extract_categories_from_footer_text(self, footer_text):
		'''Footer text format:
		This entry was posted in [categories separated by comma] and tagged [tags separated by commas]
		on [Date]. 
		Attempting to solve by splits of text strings
		'''
		first_split = footer_text.split('This entry was posted in');
		second_split = first_split[1].split(' on ');
		third_split = second_split[0].split('and tagged');
		fourth_split = third_split[0].split(',');
		categories = [x.strip(' ') for x in fourth_split]
		self.logs(categories);
		return categories;

	def search_get_search_entry(self, driver):
		return self.get_element(driver, 'search_field_entry_field');

	def is_search_entry_field_exists(self, driver):
		if self.get_elements(driver, 'search_field_entry_field'):
			return True;
		return False;

	def search_get_search_submit_button(self, driver):
		return self.get_element(driver, 'search_field_submit_button');

	def is_search_submit_button_exists(self, driver):
		if self.get_elements(driver, 'search_field_submit_button'):
			return True;
		return False;

	def search_set_search_entry(self, driver, entry):
		self.search_get_search_entry(driver).send_keys(entry);

	def search_submit_search(self, driver):
		self.search_get_search_submit_button(driver).click();

	def get_search_search_results_article_hyperlinks(self, driver):
		links = self.get_elements(driver, 'entry_entry_title_element_a')
		hyperlinks = [link.get_property('href') for link in links]
		return hyperlinks;


	'''Menu'''
	def get_blog_menu_link(self, driver):
		return self.get_element(driver, 'blog_menu_link');

	def click_blog_menu_link(self, driver):
		self.get_blog_menu_link(driver).click();
	'''----'''

