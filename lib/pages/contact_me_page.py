import selenium
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from lib.base_page import BasePage
import time

class ContactMePage(BasePage):

	PROPERTIES_FILE = "contact_me_page.properties"

	def get_content(self, driver):
		return self.get_element(driver, 'entry_content');

	def get_content_text(self, driver):
		self.logs(self.get_content(driver).text);

	# your name

	def get_your_name_form_field(self, driver):
		return self.get_element(driver, 'form_field_your_name');

	def set_your_name_form_field(self, driver, text):
		self.get_your_name_form_field(driver).send_keys(text);

	def is_your_name_form_field_validation_text_exists(self, driver):
		return self.is_element_exist(driver, 'form_field_your_name_validation');

	def get_your_name_form_field_validation_text(self, driver):
		return self.get_element(driver, 'form_field_your_name_validation').text;

	# your email

	def get_your_email_form_field(self, driver):
		return self.get_element(driver, 'form_field_your_email');

	def set_your_email_form_field(self, driver, text):
		self.get_your_email_form_field(driver).send_keys(text);

	def is_your_email_form_field_validation_text_exists(self, driver):
		return self.is_element_exist(driver, 'form_field_your_email_validation');

	def get_your_email_form_field_validation_text(self, driver):
		return self.get_element(driver, 'form_field_your_email_validation').text;		

	# your company

	def get_your_company_form_field(self, driver):
		return self.get_element(driver, 'form_field_your_company');

	def set_your_company_form_field(self, driver, text):
		self.get_your_company_form_field(driver).send_keys(text);

	# reason for contact

	def get_reason_for_contact_form_field(self, driver):
		return self.get_element(driver, 'form_field_reason_for_contact');

	def set_reason_for_contact_form_field(self, driver, value):
		Select(self.get_reason_for_contact_form_field(driver)).select_by_value(value);

	# your message

	def get_your_message_form_field(self, driver):
		return self.get_element(driver, 'form_field_your_message');

	def set_your_message_form_field(self, driver, text):
		self.get_your_message_form_field(driver).send_keys(text);

	def is_your_message_form_field_validation_text_exists(self, driver):
		return self.is_element_exist(driver, 'form_field_your_message_validation');

	def get_your_message_form_field_validation_text(self, driver):
		return self.get_element(driver, 'form_field_your_message_validation').text;	

	# captcha

	def get_captcha_form_field(self, driver):
		return self.get_element(driver, 'form_field_captcha');		

	def set_captcha_form_field(self, driver, text):
		self.get_captcha_form_field(driver).send_keys(text);

	def is_captcha_form_field_validation_text_exists(self, driver):
		return self.is_element_exist(driver, 'form_field_captcha_validation');

	def get_captcha_form_field_validation_text(self, driver):
		return self.get_element(driver, 'form_field_captcha_validation').text;			

	# submit button

	def get_form_field_submit_button(self, driver):
		return self.get_element(driver, 'form_field_submit_button');

	def click_form_field_submit_button(self, driver):
		self.get_form_field_submit_button(driver).submit();
