import selenium
from selenium.webdriver.firefox.webdriver import WebDriver
from lib.base_page import BasePage

class WorkExperiencePage(BasePage):

	PROPERTIES_FILE = "work_experience_page.properties"

	def get_content(self, driver):
		return self.get_element(driver, "entry_content")

	def get_content_text(self, driver):
		return self.get_content(driver).text