import os;
import selenium;
from selenium.webdriver.remote.webdriver import WebDriver as RemoteDriver
from selenium.webdriver.chrome.webdriver import WebDriver as ChromeDriver
from selenium.webdriver.firefox.webdriver import WebDriver as FirefoxDriver

from lib.base_class import BaseClass

class Driver(BaseClass):

	def get_driver_properties(self):
		sauce_user_name = os.environ["SAUCE_USERNAME"];
		sauce_access_key = os.environ["SAUCE_ACCESS_KEY"];



		ex_path_local = "http://127.0.0.1:4444/wd/hub";
		ex_path_saucelabs = "https://ondemand.saucelabs.com:443/wd/hub";
		ex_path_chromedriver = "/Users/davideynon/Projects/chromedriver";

		hard_coded_for_now = {
							"browserName":"chrome",
							"version":"latest",
							"platformName":"MAC",
							"acceptSslCerts":True,
							"tunnelIdentifier":"sc-proxy-tunnel",
							"sauce:options": {
												"username": sauce_user_name,
												"accessKey": sauce_access_key,
												"build": "build_test_framework_template_2",
												"name": "test_framework_template_1",
												"maxDuration": 1800,
												"commandTimeout":300,
												"idleTimeout":1000
												}
							};
		return hard_coded_for_now;

	def get_driver(self, driver=None):
		print ("In Driver().__init__()");
		if driver is None:
			print ("Instantiating new WebDriver() instance");
			driver = self.get_sauce_labs_connection_driver();
			driver.implicitly_wait(30);
			return driver;
		else:
			driver = driver;
			return driver;

	def get_sauce_labs_direct_connection_driver(self):
		props = self.get_driver_properties();
		return RemoteDriver(command_executor="https://ondemand.saucelabs.com:443/wd/hub", desired_capabilities=props);

	def get_sauce_labs_connection_driver(self):
		props = self.get_driver_properties();
		return RemoteDriver(command_executor="http://127.0.0.1:4445/wd/hub", desired_capabilities=props);

	def get_chrome_driver_local(self):
		return ChromeDriver(executable_path="/Users/davideynon/Projects/chromedriver");

	def get_firefox_driver_local(self):
		return FirefoxDriver(executable_path="/Users/davideynon/Projects/geckodriver");


 
