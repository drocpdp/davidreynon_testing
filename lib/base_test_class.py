import unittest
from driver import Driver

class BaseTestClass(unittest.TestCase):

	def setUp(self):
		self.driver = Driver().get_driver()

	def tearDown(self):
		self.driver.quit()