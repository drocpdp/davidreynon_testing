# davidreynon_testing

Testing davidreynon.com.

Using virtualenv with python 3
=======

## To activate virtualenv

`source <desired-path>/bin/activate`

## To deactivate virtualenv

`deactivate`

## To run nosetests on python3 with html output

`python3 -m "nose" --with-html --html-file=/Users/.../FILENAME.html`

## crontab entry

`04 * * * * /Users/davideynon/Projects/davidreynon_testing/util/scripts/launch_script.sh`
